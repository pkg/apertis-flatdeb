apertis-flatdeb (0.2023.1) apertis; urgency=medium

  * Refresh the automatically detected licensing information
  * Set debian/source/format to '3.0 (native)'
  * Fix format of debian/copyright

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Fri, 04 Aug 2023 14:05:30 +0200

apertis-flatdeb (0.2023.0) apertis; urgency=medium

  * Update debian/apertis/copyright

 -- Vignesh Raman <vignesh.raman@collabora.com>  Tue, 13 Jun 2023 17:22:55 +0530

apertis-flatdeb (0.2022.1) apertis; urgency=medium

  * Add debian/apertis/lintian to enable lintian job
  * Fix script-not-executable warning

 -- Vignesh Raman <vignesh.raman@collabora.com>  Tue, 24 Jan 2023 16:47:19 +0530

apertis-flatdeb (0.2022.0) apertis; urgency=medium

  * Fix compatibility with rust-findutils

 -- Ryan Gonzalez <ryan.gonzalez@collabora.com>  Thu, 27 Jan 2022 12:10:00 -0600

apertis-flatdeb (0.2021.7) apertis; urgency=medium

  * Pass --allow-remove-essential to apt install

 -- Ryan Gonzalez <ryan.gonzalez@collabora.com>  Thu, 18 Nov 2021 11:05:00 -0600

apertis-flatdeb (0.2021.6) apertis; urgency=medium

  * Fix a quoting error in the runtime YAML

 -- Ryan Gonzalez <ryan.gonzalez@collabora.com>  Wed, 04 Nov 2021 14:33:00 -0600

apertis-flatdeb (0.2021.5) apertis; urgency=medium

  * Unbreak 'runtimes' command
  * Add the ability for one runtime to extend another
  * Add support for SDK- and platform-spefic pre_apt_script
  * Add --override-packages
  * Add support for separating locales
  * Change the extension of bundles to .flatpak
  * Add support for custom overlays
  * Add support for persisting the font cache
  * Speed up source package downloads when one is unavailable
  * Fix building bundles for alternate architectures
  * Avoid trying to download source versions that don't exist

 -- Ryan Gonzalez <ryan.gonzalez@collabora.com>  Tue, 03 Nov 2021 08:45:00 -0600

apertis-flatdeb (0.2021.4) apertis; urgency=medium

  * Provide option for running without fakeroot.
  * Fix crash when building arm64 .deb-based apps.

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Tue, 02 Nov 2021 18:24:27 +0100

apertis-flatdeb (0.2021.3) apertis; urgency=medium

  * Enable ED25519 signature of generated components.

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Thu, 07 Oct 2021 18:07:19 +0200

apertis-flatdeb (0.2021.2) apertis; urgency=medium

  * Add Priority: optional to package

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Tue, 14 Sep 2021 16:45:16 +0530

apertis-flatdeb (0.2021.1) apertis; urgency=medium

  * Merge latest upstream version into apertis-flatdeb.

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Thu, 17 Jun 2021 17:09:46 +0200

apertis-flatdeb (0.2021.0) apertis; urgency=medium

  [ Andrej Shadura ]
  * Try opening some files from a location in /usr/share
  * Add suites/v2022dev1.yaml

  [ Emanuele Aina ]
  * debian/apertis/gitlab-ci.yml: Drop since we use an external definition

 -- Emanuele Aina <emanuele.aina@collabora.com>  Tue, 13 Apr 2021 10:10:35 +0000

apertis-flatdeb (0~git20200305+cf58e46+0co1) apertis; urgency=medium

  * Initial release.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Thu, 07 Jan 2021 14:09:17 +0100
