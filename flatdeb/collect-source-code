#!/usr/bin/python3

# flatdeb — build Flatpak runtimes from Debian packages
#
# Copyright © 2016-2017 Simon McVittie
# Copyright © 2017-2018 Collabora Ltd.
#
# SPDX-License-Identifier: MIT
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Fetch source code for packages installed in the given sysroot.
"""

import argparse
import glob
import logging
import os
import re
import subprocess
import sys

from debian.debian_support import Version

try:
    import typing
except ImportError:
    pass
else:
    typing  # silence "unused" warnings


logger = logging.getLogger('flatdeb.collect-source-code')


class InstalledPackage:
    def __init__(self, fields):
        # type: (typing.Sequence[str]) -> None
        self.binary = fields[0]
        self.binary_version = fields[1]
        self.source = fields[2]

        if self.source.endswith(')'):
            self.source, self.source_version = (
                self.source.rstrip(')').split(' ('))
        else:
            self.source_version = self.binary_version

            if not self.source:
                self.source = self.binary

        self.installed_size = fields[3]

    def __str__(self):
        # type: () -> str
        return '{}_{}'.format(self.binary, self.binary_version)

    def __hash__(self):
        # type: () -> int
        return hash(self.binary) ^ hash(self.binary_version)

    def __eq__(self, other):
        # type: (typing.Any) -> bool
        if isinstance(other, InstalledPackage):
            return (
                self.binary,
                self.binary_version,
            ) == (
                other.binary,
                other.binary_version,
            )
        else:
            return NotImplemented


class SourceRequired:
    def __init__(self, source, source_version):
        # type: (str, Version) -> None
        self.source = source
        self.source_version = source_version

    def __str__(self):
        # type: () -> str
        return 'src:{}_{}'.format(self.source, self.source_version)

    def __hash__(self):
        # type: () -> int
        return hash(self.source) ^ hash(self.source_version)

    def __eq__(self, other):
        # type: (typing.Any) -> bool
        if isinstance(other, SourceRequired):
            return (
                self.source,
                self.source_version,
            ) == (
                other.source,
                other.source_version,
            )
        else:
            return NotImplemented

    def __lt__(self, other):
        # type: (typing.Any) -> bool
        if isinstance(other, SourceRequired):
            return (
                self.source,
                Version(self.source_version),
            ) < (
                other.source,
                Version(other.source_version),
            )
        else:
            return NotImplemented

    @property
    def get_source(self):
        # type: () -> str
        return '{}={}'.format(self.source, self.source_version)


def gather_sources_index(in_chroot, sysroot):
    index = set()

    for sourcelist in glob.glob(os.path.join(
            sysroot, 'var/lib/apt/lists/*_Sources.*')):
        logger.info('Indexing available sources in %s', sourcelist)

        rel_sourcelist = '/' + os.path.relpath(sourcelist, sysroot)
        sources = subprocess.check_output(
            in_chroot + ['/usr/lib/apt/apt-helper', 'cat-file', rel_sourcelist],
            universal_newlines=True,
        )

        source = None
        for line in sources.splitlines():
            if not line:
                assert source is None, 'Package {} without version'.format(
                    source)
                continue

            if line.startswith('Package:'):
                source = line.split(':', 1)[1].strip()
            elif line.startswith('Version:'):
                version = line.split(':', 1)[1].strip()

                assert source is not None, 'Version {} without package'.format(
                    version)
                index.add(SourceRequired(source, version))

                source = None

    return index


def read_manifest(path):
    # type: (str) -> typing.List[InstalledPackage]

    ret = []

    with open(path, encoding='utf-8') as reader:
        for line in reader:
            line = line.rstrip('\n')

            if not line:
                continue

            if line.startswith('#'):
                continue

            assert '\t' in line, repr(line)
            ret.append(InstalledPackage(line.rstrip('\n').split('\t')))

    return ret


def read_built_using(path):
    # type: (str) -> typing.Set[SourceRequired]

    ret = set()

    with open(path, encoding='utf-8') as reader:
        for line in reader:
            line = line.rstrip('\n')

            if line.startswith('#'):
                continue

            package, source, version = line.split('\t')
            s = SourceRequired(source, version)
            logger.info(
                '%s was Built-Using %s',
                package, s)
            ret.add(s)

    return ret


def install_source_packages(in_chroot, sources, included, missing_sources):
    try:
        subprocess.check_call(in_chroot + [
            'sh', '-euc',
            'dir="$1"; shift; mkdir -p "$dir"; cd "$dir"; "$@"',
            'sh',                       # argv[0]
            '/src/files',               # working directory
            'apt-get', '-y', '--download-only', '-q', '-q',
            '-oAPT::Get::Only-Source=true', 'source',
        ] + [s.get_source for s in sources])
    except subprocess.CalledProcessError:
        if len(sources) == 1:
            s = sources[0]

            logger.warning('Unable to download source code for %s',
                    s.get_source)

            missing_sources.add(s)
            subprocess.call(in_chroot + [
                'apt-cache', 'showsrc', s.source,
            ])

            return 1
        else:
            logger.warning(
                'Unable to download %d source(s) as a batch, trying '
                'to split them in half', len(sources))

            pivot = len(sources) // 2
            attempts = 1

            for half in sources[:pivot], sources[pivot:]:
                if half:
                    attempts += install_source_packages(
                        in_chroot, half, included, missing_sources
                    )

            return attempts
    else:
        included |= set(sources)
        return 1


def main():
    # type: (...) -> None
    parser = argparse.ArgumentParser(
        description='Collect source code',
    )
    parser.add_argument('--output', '-o', default='')
    parser.add_argument('--strip-source-version-suffix', default='')
    parser.add_argument('sysroot')

    args = parser.parse_args()

    strip_source_version_suffix = None

    if args.strip_source_version_suffix:
        strip_source_version_suffix = re.compile(
            '(?:' + args.strip_source_version_suffix + ')$')

    in_chroot = [
        'systemd-nspawn',
        '--directory={}'.format(args.sysroot),
        '--as-pid2',
        '--tmpfs=/run/lock',
        '--register=no',
    ]

    if args.output:
        in_chroot.append(
            '--bind={}:/src/files'.format(os.path.abspath(args.output))
        )

    in_chroot.append('env')

    for var in ('ftp_proxy', 'http_proxy', 'https_proxy', 'no_proxy'):
        if var in os.environ:
            in_chroot.append('{}={}'.format(var, os.environ[var]))

    manifest = os.path.join(args.sysroot, 'usr', 'manifest.dpkg')
    platform_manifest = os.path.join(
        args.sysroot, 'usr', 'manifest.dpkg.platform')
    built_using = os.path.join(
        args.sysroot, 'usr', 'manifest.dpkg.built-using')
    platform_built_using = os.path.join(
        args.sysroot, 'usr', 'manifest.dpkg.built-using.platform')

    sources_index = gather_sources_index(in_chroot, args.sysroot)

    sdk_packages = read_manifest(manifest)
    packages = sdk_packages[:]
    sources_required = set()

    if os.path.exists(platform_manifest):
        platform_packages = read_manifest(platform_manifest)
    else:
        platform_packages = []

    for p in platform_packages:
        logger.info('Package in Platform: %s', p)

        if p not in sdk_packages:
            logger.warning('Package in Platform but not SDK: %s', p)
            packages.append(p)

    for p in sdk_packages:
        if p not in platform_packages:
            logger.info('Additional package in SDK: %s', p)

    for p in packages:
        sources_required.add(SourceRequired(p.source, p.source_version))

    sources_required |= read_built_using(built_using)

    if os.path.exists(platform_built_using):
        sources_required |= read_built_using(platform_built_using)

    sources = set()             # type: typing.Set[SourceRequired]
    get_source = []             # type: typing.List[str]
    included = set()            # type: typing.Set[SourceRequired]

    for s in sources_required:
        source = s.source
        source_version = s.source_version

        # TODO: Is this necessary any more?
        source = source.split(':', 1)[0]

        if strip_source_version_suffix is not None:
            source_version = strip_source_version_suffix.sub(
                '', source_version)

        s = SourceRequired(source, source_version)
        sources.add(s)
        get_source.append(s.get_source)

    missing_sources = sources - sources_index
    sources &= sources_index

    attempts = install_source_packages(
        in_chroot, list(sources), included, missing_sources)
    logger.info('Downloaded %d source packages after %d attempt(s)',
        len(included), attempts)

    parent = args.output or os.path.join(args.sysroot, 'src', 'files')

    with open(
        os.path.join(parent, 'Sources'), 'w'
    ) as writer:
        subprocess.check_call(in_chroot + [
            'sh', '-euc',
            'dir="$1"; shift; mkdir -p "$dir"; cd "$dir"; "$@"',
            'sh',                       # argv[0]
            '/src/files',               # working directory
            'dpkg-scansources',
            '.',
        ], stdout=writer)

    with open(
        os.path.join(parent, 'Sources.gz'), 'wb'
    ) as binary_writer:
        subprocess.check_call([
            'pigz', '-c', '-n', '--rsyncable',
            os.path.join(parent, 'Sources'),
        ], stdout=binary_writer)

    os.remove(os.path.join(parent, 'Sources'))

    try:
        with open(
            os.path.join(parent, 'sources.txt'), 'r'
        ) as reader:
            for line in reader:
                if line.startswith('#'):
                    continue

                source, source_version = line.rstrip('\n').split('\t')[:2]
                included.add(SourceRequired(source, source_version))
    except OSError:
        pass

    with open(
        os.path.join(parent, 'sources.txt'), 'w'
    ) as writer:
        writer.write('#Source\t#Version\n')

        for s in sorted(included):
            writer.write('{}\t{}\n'.format(s.source, s.source_version))

    if missing_sources:
        logger.warning('Missing source packages:')

        try:
            with open(
                os.path.join(parent, 'MISSING.txt'), 'r'
            ) as reader:
                for line in reader:
                    if line.startswith('#'):
                        continue

                    source, source_version = line.rstrip('\n').split('\t')[:2]
                    missing_sources.add(SourceRequired(source, source_version))
        except OSError:
            pass

        with open(
            os.path.join(parent, 'MISSING.txt'), 'w'
        ) as writer:
            writer.write('#Source\t#Version\n')

            for s in sorted(missing_sources):
                logger.warning('- %s', s)
                writer.write('{}\t{}\n'.format(s.source, s.source_version))

        logger.warning('Check that this runtime is GPL-compliant!')


if __name__ == '__main__':
    if sys.stderr.isatty():
        try:
            import colorlog
        except ImportError:
            pass
        else:
            formatter = colorlog.ColoredFormatter(
                '%(log_color)s%(levelname)s:%(name)s:%(reset)s %(message)s')
            handler = logging.StreamHandler()
            handler.setFormatter(formatter)
            logging.getLogger().addHandler(handler)
    else:
        logging.basicConfig()

    logging.getLogger().setLevel(logging.DEBUG)

    try:
        main()
    except KeyboardInterrupt:
        raise SystemExit(130)
    except subprocess.CalledProcessError as e:
        logger.error('%s', e)
        raise SystemExit(1)
